Title: Qu'est-ce qu'un processus ?
Author: David Delassus
Date: 2013-01-30 18:00:00
Tags: os, asm

On parle souvent en informatique de **processus**, nous allons voir au travers
de cet article ce qui se cache derrière ce terme.

## Un programme, une tâche

Lorsque l'utilisateur désire exécuter un programme, en général celui ci double-click
dessus, ou tape son nom (ou son chemin) dans la console.
Derrière ce procédé se cache tout un immense mécanisme :

* l'utilisateur va demander au système d'exploitation l'exécution du programme ;
* le système d'exploitation va charger le programme et parser sa structure, afin de repérer les _headers_ (contenant des informations définies par le compilateur
qui a assemblé le programme), les données et le code exécutable ;
* le système d'exploitation va placer les différentes structures à des endroits précis dans la RAM :
    * le code sera placé dans le **segment de code** (défini par la valeur du registre **CS** ou **ECS** sur du _32-bits_) ;
    * les données seront placées dans le **segment de données** (défini par la valeur du registre **DS** ou **EDS**) ;
* le système d'exploitation va créer le tas (_heap_ en anglais) et la pile (définie par la valeur du registre **SS** ou **ESS**) ;
* le système d'exploitation va ensuite initialiser les différents pointeurs :
    * **SP** (ou **ESP**) : pointera la fin du segment de pile (le sommet de la pile), il sera décrémenté pour chaque instruction ``PUSH`` et incrémenté pour chaque instruction ``POP`` ;
    * **IP** (ou **EIP**) : pointera vers le début du segment de code, il sera incrémenté après chaque instruction exécutée, et modifié pour chaque instruction ``JMP`` ou ``CALL`` ;
    * **BP** (ou **EBP**) : pointera vers le début du segment de pile (le début du tas), c'est le développeur qui se charge d'utiliser ce pointeur.
* l'exécution peut enfin se dérouler.

L'ensemble de cette structure (représenté par le schéma ci-dessous) est appelé : une **tâche**.

<center>![schéma d'un processus](http://ompldr.org/vaGFkZg.png)</center>

_NB:_ _text_ est notre segment de code, _bss_ une extension du segment de données.

## Une tâche, un processus

Associée à cette tâche, le système d'exploitation créé une nouvelle structure, comprenant :

* un identifiant : qui permet d'identifier la tâche parmi les autres ;
* une pile : chaque tâche possède sa propre pile (définie par **SS**/**ESS**) ;
* une page mémoire (ou plus si nécessaire) ;
* d'autres propriétés qu'il n'est pas nécessaire de décrire ici (priorité, limitation, ...).

Cette structure forme le contexte de notre tâche, que l'on appelle : **le processus**.

Le système d'exploitation va donc pouvoir grâce à cette structure _switcher_ d'une tâche à l'autre selon un algorithme de répartition du temps CPU, c'est **l'ordonnanceur**, la pierre angulaire du multitâche, son principe de fonctionnement est simple :

* on exécute pendant un temps défini la tâche (ce temps est défini par l'algorithme d'ordonnancement choisi, je vous laisse consulter [l'article Wikipédia](https://fr.wikipedia.org/wiki/Ordonnancement_dans_les_syst%C3%A8mes_d'exploitation#Types_d.27algorithmes)) ;
* une fois le temps d'exécution écoulé, on coupe les interruptions (instruction ``CLI``) ;
* on sauvegarde l'état des registres dans la structure du processus ;
* on sélectionne la tâche à exécuter, et on récupère la valeur des registres depuis la structure du processus associé ;
* on défini les registres avec les valeurs récupérées précédemment ;
* on réactive les interruptions (instruction ``STI``) ;
* on exécute la nouvelle tâche pendant un temps défini (retour au premier point).

## Un peu d'assembleur

### L'instruction JMP et CALL

Je vous ai parlé il y a peu des instructions ``JMP`` et ``CALL``, en vous disant qu'elles avaient un effet sur le registre **IP**/**EIP**.
Mais je ne vous en ai pas dit plus.

Voyons leurs effet :

    :::asm
    _start:      
        mov ax, 5
        jmp monlabel2   ; Cette instruction aura pour effet de placer l adresse de monlabel2 dans EIP
        ; la prochaine instruction à être exécuté est donc au label monlabel2

    monlabel1:
        xor ax, ax
        ret

    monlabel2:
        mov bx, ax
        jmp monlabel1   ; Cette instruction aura pour effet de placer l adresse de monlabel1 dans EIP
        ; la prochaine instruction à être exécuté est donc au label monlabel1

L'instruction ``JMP`` modifie donc directement le pointeur d'instruction, permettant ainsi
de faire un saut à un autre endroit de la mémoire.

Contrairement à ``JMP``, l'instruction ``CALL`` va empiler la valeur du registre **EIP** avant le saut :

    :::asm
    call monlabel

    ; équivaut donc à :
    push eip
    jmp monlabel

Pour revenir à l'instruction suivant l'instruction ``CALL``, un simple ``RET`` suffit, son effet est de
dépiler sur la pile, pour récupérer la valeur du registre **EIP** et y sauter :

    :::asm
    ret

    ; équivaut donc à
    pop eax
    mov eip, eax
    ; pas besoin de JMP, ici, eip va être incrémenté et pointera donc sur l'instruction suivant le CALL

Cette assimilation est simpliste et erronée car ``CALL`` et ``RET`` sont des **opcodes** définis, mais
cela permet de bien comprendre leurs comportements.

L'instruction ``CALL`` permet donc de créer des fonctions en assembleur (couplé à l'instruction ``RET``)

### Des fonctions, oui mais avec des arguments

On a deux écoles en _Assembleur_, ceux qui passent leurs arguments via les registres :

    :::asm
    mov eax, 5
    mov ebx, 4
    call mafonction

    ; ...

    ; résultat dans EAX
    mafonction:
        mov edx, ebx
        add edx

        ret

Et ceux qui passent leurs arguments via la pile :

    :::asm
    push 4
    push 5
    call mafonction

    mafonction:
        pop edx
        pop eax
        add edx

        push eax

        ret

**STOOOOOOP !**

Vous ne voyez pas ? On ``POP`` ! Mais, je vous rappelle que **EIP** est sur la pile,
comment va faire ``RET`` ?

Ce n'est pas la bonne solution, voici la bonne solution :

    :::asm
    push 4
    push 5
    call mafonction

    ; on a passé deux arguments sur la pile
    ; il faut les dépiler, mais on s'en fou des valeurs
    ; alors on modifie directement ESP
    ; comme la pile stocke des entiers sur 32 bits, soit 4 octets
    ; on lui ajoute 2*4 pour remonter de deux cases mémoires.
    add esp, 8

    mafonction:
        ; On sauvegarde le registre EBP sur la pile, on va s'en servir
        ; pour accéder à la pile sans push et pop
        push ebp
        mov ebp, esp ; on stocke le sommet de la pile dans notre pointeur.

        pusha ; on sauvegarde l'état des registres !
        pushf ; on sauvegarde le registre de flags !

        ; ici ESP a changé, mais ce n'est pas grave, on a ce qui nous intéresse dans
        ; EBP

        mov eax, [ebp + 4]  ; premier argument !
        mov edx, [ebp + 8]  ; second argument !
        add edx

        popf  ; on restaure le registre de flags
        popa  ; on restaure l'état des registres

        mov esp, ebp ; on restaure l'état initial de ESP
        pop ebp      ; puis on restaure l'état initial de EBP

        ret          ; enfin, on peut retourner

_NB:_ On peut faire ``ret 8``, ce qui évite le ``add esp, 8``.

_NB:_ L'utilisation de ``pusha pushf`` et de ``popf popa`` empêche tout retour via les registres.
C'est utilisé dans le cas où la fonction ne renvoie rien, enlevez ces lignes si vous souhaitez
retourner quelque chose via les registres.

## Conclusion

Voilà un petit tour de ce qu'est un processus, avec un petit peu d'assembleur pour
bien comprendre le concept de pile et d'adresse.

Dans le prochain article, nous parlerons de parallélisme en C avec ``fork()``, il
est pour cela nécessaire de bien avoir compris le contenu de cet article.
