Title: Intégration de Python en C
Date: 2013-01-29 17:35:00
Tags: c, python
Author: David Delassus

L'intégration de Python en C peut être utile dans différents cas :

* scripter l'application (configuration) ;
* système de plugins ;
* scinder l'application en deux parties (UI, coeur) ;
* faciliter le développement de certaines tâches.

Cette intégration se fait en exposant certaines structures de données ou fonctions
en tant que module Python, et vice versa, c'est à dire rendre accessible au
programme C les structures de données ou fonctions du script au programme.

## L'API C de Python

On commence par inclure le header Python :

    :::c
    #include <Python.h>

Ceci nous offre la possibilité de créer diverse structures de données propres à
Python :

    :::c
    /* création d'un tuple contenant 3 éléments */
    PyObject *tuple = PyTuple_New (3);

    /* tuple[0] = 1 */
    PyTuple_SetItem (tuple, 0, PyInt_FromLong (1L));

    /* tuple[1] = 2 */
    PyTuple_SetItem (tuple, 1, PyInt_FromLong (2L));

    /* tuple[2] = "three" */
    PyTuple_SetItem (tuple, 2, PyString_FromString ("three"));

Un autre exemple :

    :::c
    PyObject *tuple, *list;

    tuple = Py_BuildValue ("(iis)", 1, 2, "three");
    list  = Py_BuildValue ("[iis]", 1, 2, "three");

Pour plus d'informations sur l'API, je vous renvoi vers la [documentation](http://docs.python.org/2/c-api/)

## Exécuter Python dans un programme C

Voici un exemple très simple :

    :::c
    #include <Python.h>

    int main (int argc, char **argv)
    {
        Py_SetProgramName (argv[0]); /* pas obligatoire mais recommandé, cela va définir __name__ */

        Py_Initialize ();

        PyRun_SimpleString (
            "from time import time, ctime\n"
            "print 'Today is', ctime(time())\n"
        );

        Py_Finalize ();

        return 0;
    }

Maintenant, on sait comment exécuter un script au sein de notre application.

## Importer, exécuter

Imaginons que nous ayons un module Python que l'on désire utiliser.
Par exemple :

    :::python
    # multiply.py

    def multiply(a, b):
        print "Will compute", a, "times", b

        c = 0
        for i in range(0, a):
            c = c + b

        return c

Nous allons donc importer le module ``multiply``, appeler la fonction
``multiply()`` et terminer le programme.

    :::c
    #include <Python.h>

    int main (int argc, char **argv)
    {
        PyObject *module_name = NULL;
        PyObject *module      = NULL;

        if (argc < 3)
        {
            /* stdio.h, stdlib.h, string.h, ... sont inclus par Python.h */
            fprintf (stderr, "Usage: call <python module> <function> [args]\n");
            return EXIT_FAILURE;
        }

        /* on initialise l'environnement Python */
        Py_Initialize ();

        /* On récupère le nom du module dans un objet string compris par Python */
        module_name = PyString_FromString (argv[1]);

        /* on importe le module
         *
         * import <module_name>
         */
        module = PyImport_Import (module_name);

        /* On a plus besoin de l'objet module_name !
         * nous allons donc supprimer la référence que nous avons dessus,
         * afin que le Garbage Collector de Python puisse le supprimer
         */
        Py_DECREF (module_name);

        if (module != NULL)
        {
            /* On récupère la fonction présente dans le module,
             * il s'agit ici de faire :
             *
             * function = getattr (<module>, <function name>)
             */
            PyObject *function = PyObject_GetAttrString (module, argv[2]);

            /* on vérifie que notre fonction existe, et qu'il s'agit bien
             * d'un objet que l'on peut appeler.
             */
            if (function && PyCallable_Check (function))
            {
                /* on va créer un tuple qui contiendra les arguments
                 * à passer à notre fonction.
                 */
                PyObject *args  = PyTuple_New (argc - 3);
                PyObject *value = NULL;

                int i;

                for (i = 0 ; i < argc - 3 ; ++i)
                {
                    /* on convertit l'argument de la ligne de commande en
                     * objet int, compris par Python
                     */
                    value = PyInt_FromLong (strtol (argv[i + 3], NULL, 10));

                    if (value == NULL)
                    {
                        /* il y a eu une erreur, on déréférence tout */
                        Py_DECREF (args);
                        Py_DECREF (module);

                        fprintf (stderr, "Can't convert argument to long : %s\n", argv[i + 3]);
                        return EXIT_FAILURE;
                    }

                    /* la référence vers value ici est "volée", pas besoin de la
                     * déréférencer.
                     */
                    PyTuple_SetItem (args, i, value);
                }

                /* on appelle la fonction et on récupère son retour :
                 * value = function (*args)
                 */
                value = PyObject_CallObject (function, args);

                /* on a plus besoin de args, on laisse le Garbage Collector faire */
                Py_DECREF (args);

                if (value != NULL)
                {
                    /* on affiche le résultat */
                    printf ("Result of call: %ld\n", PyInt_AsLong (value));

                    /* on a plus besoin de value */
                    Py_DECREF (value);
                }
                else
                {
                    /* une erreur a eu lieu, on déréférence tout et on l'affiche */
                    Py_DECREF (function);
                    Py_DECREF (module);

                    PyErr_Print ();

                    fprintf (stderr, "Call failed!\n");
                    return EXIT_FAILURE;
                }
            }
            else
            {
                /* la fonction n'existe pas, ou ce n'est pas une fonction */
                if (PyErr_Occured ())
                {
                    PyErr_Print ();
                }

                fprintf (stderr, "Cannot find function: %s\n", argv[2]);
            }

            /* on a plus besoin de la fonction et du module */
            Py_XDECREF (function); /* XDECREF : si function est NULL, ne fais rien */
            Py_DECREF (module);
        }
        else
        {
            /* on a pas pu charger le module */
            PyErr_Print ();

            fprintf (stderr, "Failed to load module: %s\n", argv[1]);
            return EXIT_FAILURE;
        }

        /* on termine l'environnement Python */
        Py_Finalize ();

        return EXIT_SUCCESS;
    }

On sait désormais comment appeler des fonctions Python et comment manipuler des
objets Python en C.

## Exposer un module à Python

C'est bien beau tout ça, mais un intérêt non-négligeable est de pouvoir fournir
à Python des fonctions et structures de données de notre programme !

Pour cela, on créé un module dans lequel on expose notre API :

    :::c
    #include <Python.h>

    static int numargs = 0;

    /* une fonction que l'on expose retourne toujours un PyObject* et prend
     * toujours un argument self, qui est l'objet appelant la fonction (l'objet
     * dans le cas d'une méthode, le module dans le cas d'une fonction orpheline)
     * et un argument args, qui est un tuple contenant les arguments de notre
     * fonction.
     */
    static PyObject *mymodule_numargs (PyObject *self, PyObject *args)
    {
        /* tout ce qui précède le ':' correspond au type de chacun des
         * argument attendu par notre fonction, ce qui suit est le nom
         * associé à notre fonction pour le retour d'erreur.
         */

        if (!PyArg_ParseTuple (args, ":numargs"))
        {
            return NULL;
        }

        return Py_BuildValue ("i", numargs);
    }

    /* Tableau NULL-terminated de tout les éléments présents dans notre
     * module
     */
    static PyMethodDef mymodule_methods[] =
    {
        { "numargs", mymodule_numargs, METH_VARARGS, "Return the number of arguments received by the process." },
        { NULL, NULL, 0, NULL }
    };

    int main (int argc, char **argv)
    {
        numargs = argc;

        /* on initialise l'environnement python */
        Py_Initialize ();

        /* on créé notre module */
        Py_InitModule ("mymodule", mymodule_methods);

        PyRun_SimpleString (
            "import mymodule\n"
            "print 'Number of arguments:', mymodule.numargs()\n"
        );

        Py_Finalize();

        return EXIT_SUCCESS;
    }

## Conclusion

Pour le moment, on sait exporter des fonctions/données vers Python, on sait aussi
interagir avec Python. Il reste encore à voir comment créer des objets en C pour
les exporter vers Python, ce sera l'objet d'un prochain article, car c'est une
chose assez conséquente !
