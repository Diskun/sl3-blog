Title: Traitement d'image : le Maze solver.
Author: Alexis François
Date: 2013-04-23 
Tags: c++, traitement, image, cimg


[Maze Solver](https://bitbucket.org/TrashZen/maze-solver/) est un simple solveur de labyrinthe. Il prend une image de labyrinthe (le type de labyrinthe disponible dans les magasines pour enfants) et trace la solution en rouge.
![Maze solution example](https://bitbucket.org/TrashZen/maze-solver/raw/538669b9bc51399826acfa00a95d07b06d9f4edf/resource/doc/solution.png)

# Installation
--------------

## Outils requis

Pour compiler, vous avez besoin de CMake et de la librairie X11-dev

#### Linux

    $ sudo apt-get install libx11-dev
    $ sudo apt-get install cmake

#### Mac OS
Téléchargez XQuartz (X11) [ici](http://xquartz.macosforge.org/).

Si vous n'avez pas homebrew, téléchargez le [ici](http://brew.sh) (en bas de la page), puis

    $ brew install cmake

## Compilation

    $ git clone https://bitbucket.org/TrashZen/maze-solver.git
    $ cd maze-solver
    $ cmake CMakeLists.txt
    $ make

L'exécutable est créé dans le répertoire `build/`

# Utilisation
---------------------
Lancez l'exécutable avec la commande suivante

    $ build/mazesolver

Les images d'exemples sont dans le répertoire `resource/`, donc lorsqu'il vous demande `Name of the maze to solve` écrivez par exemple `resource/maze1.bmp` (seul les images bmp sont autorisées, sauf si vous activez la gestion des JPEG et PNG (voir plus bas)). Une fois l'image affiché, cliquez sur le point de départ du labyrinthe, puis le point de fin.

Attendez quelques secondes et la solution apparait.

#### JPEG et PNG

Si vous souhaitez ouvrir des images de labyrinthes au format JPEG ou PNG et que vous disposez de la libjpeg et libpng, alors il vous suffit de décommenter les lignes suivantes dans `CMakeLists.txt` (fin du fichier)

    # target_link_libraries(mazesolver X11 pthread png jpeg)

Dans `operator.h`

    // #define cimg_use_png 1
    // #define cimg_use_jpeg 1

# Algorithmes
-------------

### Préambule

Quelques informations sur les images en gÃ©nÃ©ral ...

Une image noir et blanc est simplement un tableau de deux dimensions, chaque case correspond à un pixel.
La valeur d'une case correspond au niveau de gris : 0 correspond au noir, 255 au blanc.

Pour une image couleur, chaque pixel est définit par trois valeurs : le rouge, le vert et le bleu. Chacun étant comprit entre 0 et 255.

Afin d'ouvrir les images et de les modifier facilement, j'utilise une bibliothque nommée [CImg](http://cimg.sourceforge.net) (cool image). Elle est simple à prendre en main et plutôt complète.

Pour charger une image, il faut passer le chemin vers l'image au constructeur de la classe 



    :::c++

    CImg<unsigned char> image("lena_noir_et_blanc.bmp");


Exemple de parcourt de l'image noir et blanc utilisant :


    :::c++

    for (int y = 0; y < image.height(); ++y) {
        for (int x = 0; x < image.width(); ++x) {
            image(x, y) = 255 - image(x, y);
        }
    }


Cet exemple calcul le négatif d'une image. Pour afficher le résultat, il suffit d'appeler la méthode `display`.


    :::c++

    image.display();

### Description de Maze Solver

Pour commencer je vais d'abord ouvrir une image de labyrinthe puis la copier dans une image temporaire pour pouvoir la modifier tout en gardant l'original.


    :::c++

    ImageUchar maze_image(maze_name.c_str());
    ImageUchar tmp_image(maze_image);

<center>![Maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/maze3.bmp)</center>

Le première étape est de transformer l'image couleur (rouge, vert, bleu) en noir et blanc (image en niveau de gris). Le gris est la moyenne pondérée de chaque couleurs. On va donc passer sur chaque pixel, faire la moyenne pondérée entre le rouge, le vert et le bleu et stocker le résultat dans l'image en niveau de gris.


    :::c++

    rgb_to_grayscale(tmp_image);

<center>![Grayscale maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/1_rgb_to_grayscale.png)</center>

L'Ã©tape suivante est la binarisation de l'image, c'est Ã  dire que l'image doit contenir uniquement du blanc ou du noir, mais pas de nuances de gris. Pour cela, j'utilise un seuil : tous les pixels infÃ©rieurs au seuil seront noir (0) et tous ceux supÃ©rieur seront blanc (255). Après cette étape, j'ai une image avec les murs du labyrinthe en noir et les chemins en blanc. Attention : la valeur du seuil est très importante car avec un seuil trop bas les murs peuvent disparaitre et avec un seuil trop haut les chemins peuvent disparaitre.

Ici j'utilise un seuil de 210


    :::c++

    binarization(tmp_image, 210);

<center>![Binary maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/2_binarization.png)</center>

Afin de corriger quelques imperfections possible de l'image et de la binarisation, j'effectue une érosion sur l'image. Cela a comme conséquence d'épaissir les régions noirs, soit les murs dans notre cas. 

Erosion : chaque pixel de l'image vaut le minimum de ses voisins et de lui-même. Voici un exemple d'érosion :

<center>![Erosion example](http://www.cs.auckland.ac.nz/courses/compsci773s1c/lectures/ImageProcessing-html/mor-pri-erosion.gif)</center>

Application de l'érosion au labyrinthe :

    :::c++

    erosion(tmp_image);


<center>![Erosion maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/3_erosion.png)</center>

Je dois désormais réduire la largeur des chemins pour qu'ils ne fassent qu'un pixel de large. J'utilise pour cela un algorithme de squelettisation qui permet de récupérer le squelette d'une image. Voici un exemple visuel qui est plus parlant qu'une longue explication :

En gris claire est affiché l'image de base, et en noir son squelette.

<center>![Skeleton example](http://www-artemis.it-sudparis.eu/Artemis/Research/Retinas/images/figure6.gif)</center>

L'algorithme consiste à appliquer une succession de diffèrents éléments structurants jusqu'à la stabilité de l'image. Voici le résultat sur notre labyrinthe :

    :::c++

    skeletonization(tmp_image);

<center>![Skeleton maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/5_skeletonization.png)</center>

Dernière grosse étape : trouver le plus court chemin du point de début à l'arrivée. Pour cela j'utilise un algorithme très célèbre : le A* (astar). Voici un exemple :

<center>![A* example](http://upload.wikimedia.org/wikipedia/commons/f/f4/Pathfinding_A_Star.svg)</center>

Le pixel vert est le point de départ, avec la valeur 0. Tous ses voisins sans valeur sont affectés avec la valeur du pixel courant + 1 et ajoutés dans une `queue`. Puis on traite le premier pixel de la `queue` et on recommence.

Lorsque le point de fin est atteint (le point bleu sur l'exemple), l'algorithme est finit. Il suffit juste de suivre le chemin inverse, de la valeur maximal (19 sur l'exemple) vers 0.

    :::c++

    astar(tmp_image, start_point, end_point);


<center>![A* maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/6_astar.png)</center>

Dernière étape : superposer la solution avec l'image d'origine, en transformant le chemin blanc en chemin rouge. Et on effectue la sauvegarde de la solution.


    :::c++

    merge(tmp_image, maze_image);
    maze_image.save_bmp("solution.bmp");

<center>![Solution maze](https://bitbucket.org/TrashZen/maze-solver/raw/1e5e60f3239b4335cbf88695ae63424f4faee0ad/resource/doc/7_solution.png)</center>

Pour plus d'informations sur le traitement d'image : [Le traitement d'images](http://raphaello.univ-fcomte.fr/ig/traitementimages/TraitementImages.htm).
