Title: Traitement d'image: Trivial Steganography
Author: Alexis François
Date: 2013-09-26 00:00:00
Tags: Stéganograpie, traitement d'image


La [stéganographie](http://fr.wikipedia.org/wiki/Stéganographie) est l'art de dissimuler de l'information dans un document. L'objectif est qu'une personne qui voit le document ne se doute pas qu'une information y est dissimulée.

Tout l'enjeu réside dans le fait que même en connaissant la méthode de dissimulation, une personne récupérant l'image ne doit pas pouvoir déterminer si une information s'y cache ou non. Cependant dans un souci de simplicité nous nous limiterons à présenter un schéma pour lequel une personne ne peut distinguer à l'oeil nu si un document a été stéganographié.

Nous pouvons stocker n'importe quel type d'information tel qu'un texte, un copyright, un son ou encore une image dans un document qu'il soit textuel, sonore, ou qu'il s'agisse d'une image. 

Un schéma de stéganographie est composé de deux algorithmes : 

* Dissimulation
* Extraction

L'algorithme de dissimulation prend en paramètre un document original et le document à dissimuler, et va générer un document stéganographié. L'algorithme d'extraction va quant à lui prendre en paramètre un document stéganographié et va retourner le document dissimulé une fois extrait.

Dans cet article nous allons voir comment dissimuler une image dans une autre, en utilisant un algorithme simple mais efficace. Il consiste à modifier les pixels d'une certaine manière. Nous verrons également comment extraire l'image dissimulée. 

Pour ce faire, nous allons dans un premier temps voir quelques rappels sur le binaire et ses opérateurs. Puis nous verrons un exemple d'utilisation de la bibliothèque [CImg](http://cimg.sourceforge.net) qui permet de travailler sur des images. Ensuite nous décrirons les deux algorithmes du schéma de la stéganographie : la dissimulation et l'extraction.

Et pour finir nous vous donnerons quelques pistes pour aller plus loin dans la stéganographie.


## Le binaire

Nous allons tout d'abord voir quelques rappels sur le binaire qui seront essentiels par la suite.

Le système binaire est un système de numération utilisant la base 2. On nomme couramment bit les chiffres binaires. Ceux-ci ne peuvent prendre que deux valeurs: 0 ou 1. Un ensemble de huit chiffres binaires est appelé un octet et peut stocker 2^8 (= 256) valeurs différentes soit de 0 à 255 inclus. 

Prenons un exemple, nous souhaitons convertir 1001 1011 en décimal.
Chaque bit d'un octet correspond à une puissance de 2. Le premier bit (le plus à droite) représente 2^0 = 1, le deuxième 2^1 = 2, le troisième 2^2 = 4, le quatrième 2^3 = 8 et ainsi de suite. Il ne reste plus qu'à additionner les valeurs correspondantes aux bits à 1 pour récupérer le nombre en décimal.

Voici notre nombre binaire écrit verticalement :


```

1 -> 1 * 2^7 = 1 * 128 = 128
0 -> 0 * 2^6 = 0 * 64  =   0
0 -> 0 * 2^5 = 0 * 32  =   0
1 -> 1 * 2^4 = 1 * 16  =  16

1 -> 1 * 2^3 = 1 * 8   =   8
0 -> 0 * 2^2 = 0 * 4   =   0
1 -> 1 * 2^1 = 1 * 2   =   2
1 -> 1 * 2^0 = 1 * 1   =   1


128 + 16 + 8 + 2 + 1 = 155

```

1001 1011 correspond donc à 155.

Tous les bits d'un octet ont un poids, qui correspond à leur position dans l'octet. On appelle bits de poids fort les bits se trouvant à gauche de l'octet, car ils ont une forte influence sur la valeur de l'octet (128, 64 ...). A contrario les bits à droite sont appelés bits de poids faible car ils influent peu sur l'octet (1, 2, 4 ...).

## Les opérateurs binaires

Nous allons maintenant revoir quelques opérations binaires, qui vont nous être très utiles pour la suite.

En C++ il existe plusieurs types d'opérateurs, les plus classiques sont les opérateurs de calcul (+, -, *, /, %, ++, +=, ...), les opérateurs de comparaison (<, <=, !=, ...) et les opérateurs logiques (||, &&, !).

Mais il existe également des opérateurs bit-à-bit :

- & (ET binaire) : Retourne 1 si les deux bits de même poids sont à 1. Exemple (9 & 12) renverra 8 car en binaire cela donne (1001 & 1100 -> 1000)
- | (OU binaire) : Retourne 1 si l'un ou l'autre des deux bits de même poids est à 1 (ou les deux). Exemple (9 | 12) renverra 13 car en binaire cela donne (1001 | 1100 -> 1101)
- ^ (OU-Exclusif) : Retourne 1 si les deux bits de même poids sont différents. Exemple (9 ^ 12) renverra 5 car en binaire cela donne (1001 ^ 1100 -> 0101)

Il existe aussi deux opérateurs de décalage de bits :

- << (décalage à gauche) : Décale les bits vers la gauche et remplit les bits à droite par des 0 (multiplie par 2 à chaque décalage).
             Exemple (6 << 1) renverra 12 car on décale une fois 6 vers la gauche soit (0110 << 1 -> 1100)
- \>\> (décalage à droite) : Décale les bits vers la droite et remplit les bits à gauche par des 0 (divise par 2 à chaque décalage).
             Exemple (6 >> 2) renverra 1 car on décale deux fois 6 vers la droite soit (0110 >> 2 -> 0001)


## Le traitement d'image

En informatique, une image est représentée par une matrice (un tableau à deux dimensions). Chaque cellule de la matrice correspond à un pixel et un pixel est composé de trois couleurs : le rouge, le vert et le bleu. Les couleurs sont des entiers, dans nos exemples les valeurs seront comprises entre 0 et 255.

Par exemple le pixel ayant comme valeur (R, V, B) (255, 0, 0) sera rouge et le pixel (255, 255, 0) sera jaune.

Nous utiliserons dans cette article la bibliothèque C++ [CImg](http://cimg.sourceforge.net) pour ouvrir, lire et modifier des images. 

Voici un exemple simple d'utilisation où nous allons juste inverser les couleurs d'une image. Le rouge sera remplacé par le vert, le vert par le bleu et le bleu par le rouge.


```cpp

  // Création d'un typedef afin d'éviter de réécrire le type à chaque utilisation
  // Une ImageUchar est une image ou chaque couleur est codée sur 1 octet (unsigned char ou uint8_t)
  typedef cimg_library::CImg<uint8_t> ImageUchar;

  // Chargement de l'image
  ImageUchar image("foo.bmp");

  // Double boucle sur la hauteur et la largeur de l'image afin de parcourir tous ses pixels
  // Le pixel (0, 0) se trouve en haut à gauche de l'image
  for (int y = 0; y < image.height(); ++y) {
      for (int x = 0; x < image.width(); ++x) {
          // Récupération des trois couleurs
          uint8_t rouge = image(x, y, 0);
          uint8_t vert  = image(x, y, 1);
          uint8_t bleu  = image(x, y, 2);

          // Inversion des couleurs
          // Le rouge (0) est remplacé par le vert
          image(x, y, 0) = vert;
          // Le vert (1) est remplacé par le bleu
          image(x, y, 1) = bleu;
          // Le bleu (2) est remplacé par le rouge
          image(x, y, 2) = rouge;
      }
  }

  // Affiche l'image après l'inversion des couleurs
  image.display();

```


On observe bien sur l'image résultat que les couleurs ont été inversées.

<center>![Couleurs inversées](https://bitbucket.org/TrashZen/trivial-steganography/raw/35131b8d002f5c83360bd5ff411ceeee69bf48ec/doc/resources/inverse_couleurs.jpg)</center>


## La stéganographie, les choses sérieuses commencent !

### La dissimulation

Il existe bien entendu plusieurs schémas de stéganographie. Nous allons ici voir un algorithme simple mais efficace permettant de dissimuler une image en noir et blanc dans une image en couleur de même taille.

Cet algorithme (appelé LSB pour Least Significant Bit) consiste à modifier les bits de poids faible de chaque pixel. Cela a pour conséquence d'augmenter ou de diminuer légèrement les valeurs de rouge, vert et bleu. La couleur du pixel se trouve donc peu modifiée mais contient une partie de l'information que nous souhaitons dissimuler.

Nous appellerons `im_rvb` l'image couleur, et `im_gris` l'image à dissimuler. Chaque pixel de `im_gris` sera dissimulé dans le pixel de `im_rvb` aux mêmes coordonnées.

Pour chaque pixel, la première étape consiste à découper le pixel de `im_gris` (stocké sur un octet) en trois parties :

* Deux parties de trois bits
* Une partie de deux bits

Car 3 + 3 + 2 = 8 bits soit un octet. Ensuite nous allons remplacer les trois bits de poids faible de la composante rouge de `im_rvb` par la première partie de `im_gris`. Puis nous remplaçons les deux bits de poids faible de la composante verte de `im_rvb` par la deuxième partie de `im_gris`. Enfin nous remplacerons les trois bits de poids faible de la composante bleue de `im_rvb` par la troisième partie de `im_gris`.

Voici un exemple concret : nous allons dissimuler un pixel gris (103) dans un pixel couleur (132, 43, 84). Nous commençons par convertir 103 en binaire: 0110 0111. Puis nous le découpons en trois parties.


```

  partie 1 : 011
  partie 2 : 00
  partie 3 : 111

```

Ensuite nous convertissons les trois valeurs du pixel couleur en binaire.
  

```

  132 -> 1000 0100
  43  -> 0010 1011
  84  -> 0101 0100

```

Nous retirons les bits de poids faible du pixel couleur.
  

```

  1000 0...
  0010 10..
  0101 0...

```

Et nous les remplaçons par les trois parties extraites précédemment.
  

```

  1000 0... <- 011
  1000 0011, qui correspond à 131

  0010 10.. <- 00
  0010 1000, qui correspond à 40

  0101 0... <- 111
  0101 0111, qui correspond à 87

```


Le pixel (132, 43, 84) devient donc (131, 40, 87). On voit que la différence entre les deux est faible et ne sera pas visible à l'oeil nu.


Voici une application en C++ :

```cpp

  /**
   * Dissimuler une image en niveau de gris (image_to_hide) dans une image couleur (image_couleur).
   *
   * Soit un pixel P de image_couleur composé de trois octets, R pour le rouge, V pour le vert, B pour le bleu
   *   rouge = R0 R1 R2 R3 R4 R5 R6 R7 -> 8 bits contenant le niveau de rouge
   *   vert  = V0 V1 V2 V3 V4 V5 V6 V7 -> 8 bits contenant le niveau de vert
   *   bleu  = B0 B1 B2 B3 B4 B5 B6 B7 -> 8 bits contenant le niveau de bleu
   * 
   * et un pixel Pg de image_to_hide composé d'un octet contenant la nuance de gris
   *   gris  = G0 G1 G2 G3 G4 G5 G6 G7 -> 8 bits contenant le niveau de gris
   *
   * alors nous allons remplacer certains bits de P de la façon suivante pour dissimuler Pg dans P :
   *   rouge = R0 R1 R2 R3 R4 G0 G1 G2
   *   vert  = V0 V1 V2 V3 V4 V5 G3 G4
   *   bleu  = B0 B1 B2 B3 B4 G5 G6 G7
   *
   * @param image_to_hide [in]      Image en niveau de gris à dissimuler
   * @param image_couleur [in, out] Image original
   */
  void hide(const ImageUchar &image_to_hide, ImageUchar &image_couleur) {

    // Double boucle (une pour la hauteur, une pour la largeur) permetant de parcourir tous les pixels de image_couleur
    for (int y = 0; y < image_couleur.height(); ++y) {
      for (int x = 0; x < image_couleur.width(); ++x) {

        // La première étape consiste à découper le pixel de image_to_hide
        // (contenu sur un octet car c'est une image en noir et blanc)
        // en trois parties : la première doit faire trois bits, la deuxième deux bits et la troisième trois bits.
        // Soit 3 + 2 + 3 = 8 bits = 1 octet

        // Initialisation des trois variables contenant chacune une partie du pixel
        uint8_t part_1 = 0;
        uint8_t part_2 = 0;
        uint8_t part_3 = 0;
        
        // Vérifie que image_to_hide contient un pixel aux coordonnées (x, y)
        // Cela évite d'accéder à un pixel qui se trouve en dehors de l'image
        if (image_to_hide.containsXYZC(x, y)) {
          // image_to_hide(x, y) -> valeur du pixel au coordonnées (x, y) contenu sur un octet (8 bits)
          //
          // Récupération des trois premiers bits de l'octet dans part_1
          //
          // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
          // image_to_hide(x, y) >> 5         -> 0000 0xxx
          part_1 = image_to_hide(x, y) >> 5;

          // Récupération des deux bits suivants dans part_2
          //
          // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
          // image_to_hide(x, y) & 24         -> 000x x000
          // (image_to_hide(x, y) & 24) >> 3  -> 0000 00xx
          part_2 = (image_to_hide(x, y) &  24) >> 3;

          // Récupération des trois derniers bits dans part_3
          //
          // image_to_hide(x, y)              -> xxxx xxxx (x vaut 0 ou 1)
          // image_to_hide(x, y) & 24         -> 0000 0xxx
          part_3 =  image_to_hide(x, y) &   7;
        }


        // Remplissage des trois derniers bits de l'octet contenant le rouge à 0 (xxxx x000) (x vaut 0 ou 1)
        image_couleur(x, y, 0) &= 248; // rouge & 1111 1000

        // Remplissage des deux derniers bits de l'octet contenant le vert à 0 (xxxx xx00)
        image_couleur(x, y, 1) &= 252; // vert & 1111 1100

        // Remplissage des trois derniers bits de l'octet contenant le bleu à 0 (xxxx x000)
        image_couleur(x, y, 2) &= 248; // bleu & 1111 1000

        // Il ne reste plus qu'à insérer part_1, part_2 et part_3 dans image_couleur, 
        // une dans chaque composante (couleur).
        // L'astuce pour que la modification de l'image ne soit pas visible (ou le moins possible) est
        // de modifier les bits de poids faible,
        // ceux les plus à droite. Cela permet de faire varier la valeur de chaque couleur assez faiblement 
        // pour ne pas être visible à l'oeil nu.
        image_couleur(x, y, 0) = image_couleur(x, y, 0) | part_1; // xxxx x000 | 0000 0xxx
        image_couleur(x, y, 1) = image_couleur(x, y, 1) | part_2; // xxxx xx00 | 0000 00xx
        image_couleur(x, y, 2) = image_couleur(x, y, 2) | part_3; // xxxx x000 | 0000 0xxx
        }
      }
  }

```


Et voilà le résultat en image de cet algorithme.

<center>![Resumé](https://bitbucket.org/TrashZen/trivial-steganography/raw/35131b8d002f5c83360bd5ff411ceeee69bf48ec/doc/resources/resume.jpg)</center>


Si vous avez bien suivi, vous avez dû remarquer qu'on ne modifie que deux bits de la composante verte alors que le bleu et le rouge ont chacun trois bits modifiés. Logique car nous n'avons que huit bits à dissimuler.
Peut-être que certains d'entre vous se sont demandés pourquoi c'est le vert qui reçoit la plus faible modification et non le rouge ou le bleu ?

La réponse se trouve dans nos yeux ! Et oui, l'oeil humain est plus sensible au vert qu'au rouge ou au bleu. Il est donc préférable de modifier plus faiblement le vert que les autres composantes afin que la dissimulation de l'image soit la plus discrète possible.


### L'extraction

Nous avons vu comment dissimuler une image noir et blanc dans une autre image. Vous pouvez maintenant partager cette image sans que personne ne voit la deuxième image qui s'y cache !

Une fois que votre correspondant a récupéré cette image, il doit en extraire l'image contenue, pour cela il suffit d'appliquer l'algorithme dans l'autre sens.

Il faut récupérer les trois derniers bits de la composante rouge, les deux derniers de la composante verte et les trois derniers de la composante bleue. Une fois les trois parties récupérées, il ne reste plus qu'à les mettre bout-à-bout pour retrouver le pixel noir et blanc de l'image dissimulée.


Voici l'application en C++ :


```cpp

  /**
   * Extraire l'image en niveau de gris dissimulée dans une image
   * Il faut appliquer l'algorithme inverse pour retrouver l'image cachée.
   *
   * @param image           [in]  Image stéganographiée
   * @param image_extracted [out] Image en niveau de gris
   */
  void extract(const ImageUchar &image, ImageUchar &image_extracted) {
    for (int y = 0; y < image.height(); ++y) {
      for (int x = 0; x < image.width(); ++x) {
        // Récupération des trois derniers bits contenus dans le rouge
        uint8_t part_1 = (image(x, y, 0) & 7) << 5; //0000 0111

        // Récupération des deux derniers bits contenus dans le vert
        uint8_t part_2 = (image(x, y, 1) & 3) << 3; //0000 0011

        // Récupération des trois derniers bits contenus dans le bleu
        uint8_t part_3 =  image(x, y, 2) & 7;       //0000 0111

        // Fusion de part_1, part_2 et part_3 afin de recomposer le niveau de gris original
        image_extracted(x, y) = (part_1 | part_2) | part_3;
      }
    }
  }

```


Résultat de cet algorithme sur l'image précédemment obtenue. Nous obtenons bien l'image noir et blanc préalablement dissimulée.


<center>![Résumé](https://bitbucket.org/TrashZen/trivial-steganography/raw/35131b8d002f5c83360bd5ff411ceeee69bf48ec/doc/resources/resume_extraction.jpg)</center>


## Aller plus loin

D'autres techniques similaires existent. Par exemple nous pouvons changer l'espace de colorisation [RVB](http://fr.wikipedia.org/wiki/Rouge_vert_bleu) (Rouge Vert Bleu) en [TSL](http://fr.wikipedia.org/wiki/Teinte_Saturation_Luminosité) (Teinte Saturation Luminance) et ainsi modifier la teinte, la saturation et la luminance plutôt que le niveau de rouge, de vert et de bleu.

L'inconvénient de ces algorithmes est qu'ils ne permettent aucune dégradation de l'image. Le simple fait de compresser l'image en JPEG est susceptible de détruire l'image dissimulée. Cela est dû au format JPEG en lui-même car il est basé sur un algorithme de compression avec perte. Les bits de poids faible vont être modifiés et l'image dissimulée détruite. Le redimensionnement de l'image aura le même effet.

Pour pallier à ces problèmes, d'autres algorithmes existent, basés sur le domaine fréquentiel. Un algorithme célèbre consiste à appliquer une transformation de Fourier sur notre image, modifier ensuite les fréquences moyennes et effectuer une transformation inverse. 

Pourquoi modifier les fréquences moyennes et pas les autres ?

Car si nous modifions les fréquences faibles, la première compression venue va détruire l'image dissimulée. Et si nous modifions les fréquences trop hautes, l'image originale va recevoir trop de modifications et la stéganographie sera visible. Voici un article détaillant la [transformée de Fourier](http://www.tsi.telecom-paristech.fr/pages/enseignement/ressources/beti/ondelettes-2g/francais/Fourier/TF2Dimage.htm) pour approfondir le sujet.

Si le traitement d'image vous intéresse voici un [article intéressant](http://images.math.cnrs.fr/Le-traitement-numerique-des-images.html) reprenant les bases nécessaires pour commencer à développer ses propres algorithmes.

Lien du git avec les sources : [https://bitbucket.org/TrashZen/trivial-steganography/overview](https://bitbucket.org/TrashZen/trivial-steganography/overview).