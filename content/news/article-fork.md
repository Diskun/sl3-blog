Title: Le parallélisme avec fork()
Author: David Delassus
Date: 2013-01-31 00:00:00
Tags: os, fork, asm

Dans mon [article précédent](http://www.lab-sl3.org/articles/news/quest-ce-quun-processus.html),
je vous ai parlé de processus. Cet article lui fait directement suite, car nous parlerons
ici de parallélisme. Nous aborderons la définition de parallélisme, et son implémentation en
langage C.

## Le parallélisme, c'est quoi ?

On sait que le système d'exploitation exécute différents processus en **parallèle**
grâce à l'ordonnanceur. Le parallélisme, c'est l'exécution **simultanée** (en parallèle)
de différentes parties du même programme.

Le fonctionnement est simple, le programme aura deux processus associés, au lieu
d'un. Le processus principal (créé par le système d'exploitation au lancement du
programme) est appelé **processus père**, et le processus créé par le programme
(en réalité, le programme demande au système d'exploitation la création d'un nouveau
processus) est appelé **processus fils**.

Un processus père peut avoir plusieurs processus fils, mais un processus fils ne
possède qu'un **unique** processus père.

Le processus fils, au même titre qu'un processus normal, possède sa propre plage
mémoire, son propre contexte, son propre identifiant, etc.

## Les appels systèmes

Avant de continuer, il est nécessaire de connaître la notion **d'appel système**.

Lorsque le système d'exploitation créé un processus, ce dernier s'exécute en **mode
utilisateur**. C'est à dire qu'il n'aura pas accès à une autre plage mémoire que
celles qui lui sont allouées, il n'aura pas accès au matériel, bref il est cloisonné.
C'est le **niveau d'exécution 3**.

Ceci n'est pas très pratique pour le développement, alors que le système d'exploitation,
lui s'exécute sans aucune restriction (**niveau d'exécution 0**).

<center>![schéma des niveau d'exécution](http://www.jamesmolloy.co.uk/images/rings.png)</center>

Plus le niveau d'exécution est bas, plus le processus lancé dans ce niveau d'exécution
aura de privilèges. Il est possible de configurer ces privilèges au niveau du _CPU_,
mais cela ne fait pas partie de cet article. Sachez juste que dans un système à
mémoire paginée, il est compliqué d'utiliser le niveau d'exécution 1 et 2. On pourrait
cependant imaginer la configuration suivante :

* niveau 0 : mode noyau, tout est permis ;
* niveau 1 : pour les drivers, les I/O ne sont plus permises pour les niveaux supérieurs ;
* niveau 2 : pour les drivers vidéos, l'accès à la mémoire vidéo n'est plus permise pour les niveaux supérieurs ;
* niveau 3 : mode utilisateur, pour les applications, les services, ... rien n'est permis.

Pour palier ce problème de restriction, le noyau fournis au reste du système une
_API_ pour exécuter **temporairement** un code en mode noyau (niveau d'exécution
0). On appelle cela un appel système (_syscall_ en anglais).

Pour cela, le noyau enregistre dans son _IDT_ (_I_nterrupt _D_escriptor _T_able,
l'IVT du mode protégé) une nouvelle interruption (pour le _DOS_ c'est l'interruption
**21h**, pour les unices, c'est l'interruption **80h**). Selon la valeur des
registres lors de l'exécution de l'interruption, telle ou telle fonction sera
exécutée. Par exemple, pour **Linux** :

    :::asm
    [SECTION .text]

    [GLOBAL _start]

    _start:
        mov edx, len    ; Longueur du message a afficher
        mov ecx, msg    ; Adresse du message
        mov ebx, 1      ; on ecrit dans le descripteur de fichier 1 (stdout)
        mov eax, 4      ; appel systeme numero 4 (write)
        int 0x80        ; execution de l'appel systeme

        mov eax, 1      ; appel system numero 1 (exit)
        int 0x80        ; execution de l'appel systeme

    [SECTION .data]

    msg: db 'Hello, world!', 0xA  ; notre message
    len: equ $ - msg              ; longueur du message


## Créer un processus avec fork()

La fonction ``fork()`` de la librairie standard du C est en réalité un appel
système (l'appel système 2 sous _Linux_). Voici un exemple simple d'utilisation :

    :::c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>

    int main (void)
    {
        pid_t pid = -1;

        pid = fork ();

        if (pid > 0)
        {
            printf ("Father %ld: child process created <%ld>\n", getpid (), pid);
            sleep (2);
        }
        else if (pid == 0)
        {
            printf ("Child %ld: hello world!\n", getpid ());
            sleep (1);
        }
        else
        {
            fprintf (stderr, "Can't fork()!\n");
            exit (EXIT_FAILURE);
        }

        return EXIT_SUCCESS;
    }

L'appel système ``fork()`` va créer un nouveau processus, **à partir du processus
père**, et le lancer. Dans le processus père, il renverra l'identifiant du nouveau
processus, dans le processus fils, il renverra 0, en cas d'erreur, il renvoie -1
et ``errno`` est défini.

## Fonctionnement de fork()

J'insiste sur ma phrase précédente :

> L'appel système ``fork()`` va créer un nouveau processus, **à partir du processus père**...

_Réfléchissons... Je créé un nouveau processus à partir du premier. Il me suffirait
de copier la zone mémoire du premier processus dans la zone mémoire du second !_

Ça marche, mais si votre programme fait 4 Go ?

_Hmm... Comme le code est identique pour les deux processus, il n'est pas nécessaire
de le copier ! On copiera donc seulement le segment de données et de pile._

C'est déjà mieux, on ne peut pas mieux faire ?

_C'est vrai que le segment de données contient les données statiques du programme.
C'est donc inutile aussi de les copier, vu que ces données sont statiques !_

On progresse. En réalité, ``fork()`` ne va rien copier du tout ! Il va appliquer
le principe de **copie sur écriture** (_copy-on-write_ en anglais). Avec ce
procédé, les deux processus partagerons les mêmes données, jusqu'à ce que l'un
d'entre eux modifiera ces mêmes données. À ce moment précis, les données vont être
copiées dans le processus qui effectue la modification avant d'appliquer cette
dernière.

<center>![schéma du fonctionnement du copy-on-write](http://ompldr.org/vaGFmNg/copy-on-write.png)</center>

## La communication inter-processus

L'intérêt du parallélisme, c'est de diviser l'application en différents contextes
capables de communiquer entre eux. Prenons un exemple simple, un installeur.

L'installeur va installer le logiciel (on s'en doutait...) et en parallèle notifier
l'utilisateur de l'avancement de l'installation avec une barre de progression.

C'est simpliste, mais le problème est là. Comment faire communiquer le processus
chargé de l'installation avec le processus chargé de la progression de la barre ?

### Les tubes

Un moyen simple, mais efficace, deux faire communiquer deux processus, ce sont
**les tubes** (_pipe_ en anglais) :

    :::console
    $ cat file.txt | grep "pattern"

Dans l'exemple ci-dessus, la sortie de ``cat`` est redirigée sur l'entrée de ``grep``.

Dans l'exemple ci-dessous, on voit la création d'un tube, et son usage avec ``fork()``

    :::c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <string.h>
    #include <errno.h>

    int main (void)
    {
        int fds[2] = { -1, -1 };
        pid_t pid  = -1;

        if (pipe (fds) < 0)
        {
            fprintf (stderr, "can't create pipe : %s\n", strerror (errno));
            exit (EXIT_FAILURE);
        }

        /* fds[0] : flux d'entree
         * fds[1] : flux de sortie
         *
         * Tout ce qui est ecris dans fds[1] peut etre lu dans fds[0]
         */

        /* on fork ! */
        pid = fork ();

        if (pid < 0)
        {
            fprintf (stderr, "can't fork : %s\n", strerror (errno));

            close (fds[0]);
            close (fds[1]);

            exit (EXIT_FAILURE);
        }

        /* processus fils */
        else if (pid == 0)
        {
            FILE *out = fdopen (fds[1], 'w'); /* on encapsule notre sortie dans un stream */

            close (fds[0]); /* on ferme l'entree car inutile ici */

            /* on envoie une chaine dans le tube */
            fprintf (out, "hello world!\n");

            fclose (out);
        }

        /* processus pere */
        else
        {
            char buf[20] = { 0 };

            FILE *in = fdopen (fds[0], 'r'); /* on encapsule l'entree dans un stream */

            close (fds[1]); /* on ferme la sortie car inutile ici */

            /* on lit une chaine depuis le tube */
            fread (buf, sizeof (char), 13, in);

            printf ("Received from child %ld : %s\n", pid, buf);

            fclose (in);
        }

        return EXIT_SUCCESS;
    }

On peut maintenant faire communiquer deux processus via les tubes. Avec l'utilisation
de ``(f)write()`` et ``(f)read()``, on peut faire transiter n'importe quel type de
données (le buffer étant de type ``void *``). Si vous voulez faire transiter des
données complexes, je vous conseil de les encapsuler dans une structure (attention
aux pointeurs !). Avec l'utilisation de ``select()``, vous pouvez faire des choses
encore plus poussées, je vous conseil donc également la lecture du manuel concernant
cette fonction (``man 3 select``).

### Les signaux

Dans un système _Unix_, chaque processus peut recevoir des signaux provenant du
système d'exploitation. Un signal est un message envoyé par le système d'exploitation
pour prévenir le processus d'un événement qu'il doit gérer. Par exemple, si le
système envoie le signal **SIGSEGV** à notre programme, c'est pour le notifier qu'il
a tenté d'accéder à une zone de la mémoire qui lui est interdite (erreur de segmentation),
le programme doit donc quitter sans attendre dès la réception de ce signal.

Voici la manière la plus simple de réceptionner un signal :

    :::c
    #include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <signal.h>

    /* fonction qui gère notre signal */
    void sig_handler (int sig)
    {
        printf ("Signal received : %d\n", sig);
    }

    int main (void)
    {
        /* cette structure va nous permettre de definir notre gestionnaire de signal */
        struct sigaction mysighdlr;

        mysighdlr.sa_handler = sig_handler; /* le gestionnaire */
        mysighdlr.sa_flags   = 0;           /* on ignore ce champ pour cet exemple */

        /* sa_mask correspond au masque des signaux à bloquer pendant l'execution
         * de notre gestionnaire, ici on en bloque aucun
         */
        sigemptyset (&mysighdlr.sa_mask);

        /* on mets en place notre gestionnaire */
        sigaction (SIGINT,  &sighdlr, 0);    /* SIGINT  - provoque l'interruption du programme (CTRL-C) */
        sigaction (SIGQUIT, &sighdlr, 0);    /* SIGQUIT - provoque l'arret du programme avec production d'un core-dump */
        sigaction (SIGTERM, &sighdlr, 0);    /* SIGTERM - provoque l'interruption du programme */

        /* boucle infinie, il faudra tuer le processus avec : kill -HUP */
        while (1);
    }

Vous pouvez envoyez des signaux à l'aide du logiciel **HTop** pour voir l'effet que cela a sur votre programme.

Maintenant que l'on sait réceptionner des signaux, il faudrait savoir comment en envoyer.
La réponse nous est donnée dans la section 2 du manuel (``man 2 kill``) :

    :::c
    #include <sys/types.h>
    #include <signal.h>

    /* Envoi le signal `sig` au processus `pid` */
    int kill (pid_t pid, int sig);

Pour parfaire notre communication inter-processus (_IPC_ en anglais), il ne reste
plus qu'un point à aborder : mettre tout ça en commun.

Voici donc un petit exemple :

    :::c
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <signal.h>
    #include <errno.h>

    /* cette variable globale va nous permettre d'attendre la reception d'un signal */
    volatile sig_atomic_t usr_interrupt = FALSE;

    /* le gestionnaire du signal SIGUSR1 va mettre le precedent booleen a vrai */
    void sig_handler (int sig)
    {
        usr_interrupt = TRUE;
    }

    /* fonction du processus fils */
    void child_function (void)
    {
        printf ("I'm a child <%ld> !\n", getpid ());

        /* on envoi le signal au processus pere */
        kill (getppid (), SIGUSR1);

        printf ("Good bye!\n");
        exit (EXIT_SUCCESS);
    }

    int main (void)
    {
        struct sigaction sighdlr;
        pid_t pid = -1;

        /* on cree le gestionnaire du signal */
        sighdlr.sa_handler = sig_handler;
        sighdlr.sa_flags   = 0;

        /* on veut bloquer tout les signaux pendant l'execution de notre gestionnaire */
        sigfillset (&sighdlr.sa_mask);

        sigaction (SIGUSR1, &sighdlr, 0);

        /* maintenant, on fork comme on sait le faire */
        pid = fork ();

        if (pid < 0)
        {
            fprintf (stderr, "can't fork : %s\n", strerror (errno));
            exit (EXIT_FAILURE);
        }
        else if (pid == 0)
        {
            child_function ();
            /* ne retourne pas */
        }
        else
        {
            /* tant que usr_interrupt est faux, on attend */
            while (!usr_interrupt);

            printf ("i'm done.\n");
        }

        return EXIT_SUCCESS;
    }

Dans l'exemple ci-dessus, lorsque le fils va se lancer, il va envoyer un signal à
son père, qui va le recevoir et définir le booléen ``usr_interrupt`` à ``TRUE``,
ce qui provoquera l'arrêt de la boucle, et la fin du père (le fils aura déjà
terminé son exécution).

_NB: **SIGUSR1** et **SIGUSR2** sont des signaux propres à cette utilisation._

## Conclusion

Le parallélisme est un domaine très intéressant, car il apporte la problématique
de la communication inter-processus, et il faut être en mesure de quel type de
communication (tubes ou signaux) on a besoin.

Dans le cas de ``fork()`` (qui est un appel système lent à exécuter), on a pas
beaucoup de problème si on conçoit bien son code. Cependant on se retrouve avec
deux processus distincts !

Vous vous en doutez, il y a une suite à tout cela, car le parallélisme ne s'arrête
pas là ! Vous découvrirez dans un prochain article un parallélisme moins complexe
à mettre en place : **les threads**, qui, malgré leur simplicité, apportent leurs
lots de problèmes.
