Title: Fonctionnement d'un Garbage Collector
Author: David Delassus
Date: 2013-03-01 18:00:00

Avant tout, qu'est-ce qu'un **Garbage Collector** ? La traduction en français nous donne : **éboueur**.

Le principe du _garbage collector_ est qu'il va aider à la gestion de la mémoire, à la manière d'un éboueur
qui nous aide dans l'évacuation de nos déchets.

Attention ! Cela ne veut pas dire qu'il la gère à notre place, comme un éboueur ne viendra pas chez vous pour
sortir vos poubelles.

En gros, lorsque l'on va allouer de la mémoire, le _garbage collector_ va en prendre note et garder une trace.
Lorsque l'on libère cette mémoire, le _garbage collector_ en est notifié. Le _garbage collector_ va permettre
de déterminer par qui est utilisé une certaine zone mémoire et lorsque celle-ci n'est plus utilisée par personne,
il la libère, imaginons cela :

    :::c
    int *ptr  = gc_alloc (sizeof (int)); /* on alloue de la mémoire */
    int *ptr2 = gc_ref (ptr);            /* on créé une nouvelle référence vers cette zone mémoire */

    /* stuff with ptr */

    gc_unref (ptr);                      /* ptr n'utilise plus la zone mémoire, mais ptr2 continue de l'utiliser */

    /* stuff with ptr2 */

    gc_unref (ptr2);                     /* ptr2 ne l'utilise plus non plus, plus personne ne l'utilise, on peut la libérer */


**Q: Quelle utilité de procéder comme cela ?**

_R: Et bien, cela permet plus de facilité dans le développement, on centralise en un point la gestion de la mémoire._

Autre exemple :

    :::c
    void func (int *ptr)
    {
        /* sutff with ptr */
        gc_unref (ptr);
    }

    int main (void)
    {
        int *ptr = gc_alloc (sizeof (int));

        func (gc_ref (ptr)); /* on passe une référence à la fonction */

        /* ici notre pointeur est encore valide ! */

        /* stuff with ptr */

        gc_unref (ptr);

        /* ptr n'est plus valide */
    }

## Implémenter un GC en C

Nous allons implémenter 4 fonctions pour notre _GC_ (**G**arbage **C**ollector) :

    :::c
    void *gc_alloc (size_t sz);               /* allocation de mémoire */
    void *gc_realloc (void *addr, size_t sz); /* réallocation de mémoire */
    void *gc_ref (void *addr);                /* référencement d'un pointeur */
    void gc_unref (void *addr);               /* déréférencement d'un pointeur */

### Le nécessaire

Pour cela, on aura besoin d'une liste qui comprendra notre pointeur, et son nombre de référence :

* lors de l'allocation, le nombre de référence est à 1 ;
* lors d'un référencement, on incrémente ce nombre ;
* lors d'un déréférencement, on décrémente ce nombre ;
* si ce nombre arrive à 0, plus personne ne l'utilise, on libère la mémoire.

On aurait donc :

    :::c
    struct gc_list_t
    {
        void *addr;
        int refcount;

        struct gc_list_t *next;
        struct gc_list_t *prev;
        struct gc_list_t *head; /* une liste circulaire, c'est plus simple */
    };

### Une API pour manipuler la liste du GC

Je vous la donne telle quelle, car ce n'est pas l'objet de l'article :

    :::c
    struct gc_list_t *head = NULL;

    static void _gc_list_append (struct gc_list_t *element)
    {
        /* initialize head list */
        if (head == NULL)
        {
            head = element;    /* the head is the element */
            head->head = head;
            head->next = head; /* the next element is the head */
            head->prev = head; /* the previous element is the head */

            /* this is a circular list, there is no NULL prev/next */
        }
        /* head already initialized, add it to the end */
        else
        {
            element->head = head;        /* the head don't change */
            element->next = head;        /* the next element is the head, so element is at the end of the list */
            element->prev = head->prev;  /* the previous element of the head become the current element's previous */

            head->prev->next = element;  /* the next element of the head's previous (was the head) become the current element */
            head->prev       = element;  /* the previous element of the head becomes the current element */
        }
    }

    static void _gc_list_remove (struct gc_list_t *element)
    {
        element->prev->next = element->next; /* the next element of the previous one becomes the next of the current element */
        element->next->prev = element->prev; /* the previous element of the next one becomes the previous of the current element */

        /* so element is not linked anymore in the list */

        /* do not forget to set head at NULL if it was the head. */
        if (element == head)
        {
            head = NULL;
        }

        /* then free it */
        free (element);
    }

    static struct gc_list_t *_gc_list_find (void *addr)
    {
        struct gc_list_t *tmp = head;

        /* do the search while we are not on the head */
        do
        {
            if (addr == tmp->addr)
            {
                return tmp;
            }

            tmp = tmp->next;
        } while (tmp != tmp->head);

        /* if we reach the head, we walked trough the whole list
         * without any success :(
         */

        return NULL;
    }

### Implémentation de l'allocation

Que dois faire notre allocateur ?

* allouer la mémoire, comme demandé ;
* allouer de la place dans la liste interne ;
* initialiser le compteur de référence à 1 ;
* ajouter l'élement dans la liste ;
* retourner le pointeur vers la mémoire.

Voyons cela en C :

    :::c
    void *gc_alloc (size_t sz)
    {
        void *ptr = malloc (sz);

        if (ptr != NULL)
        {
            struct gc_list_t *element = malloc (sizeof (struct gc_list_t));

            /* if we can't allocate place for the element in the list, free memory and return NULL */
            if (element == NULL)
            {
                free (ptr);
                return NULL;
            }

            /* initialize element */
            element->addr     = ptr;
            element->refcount = 1;

            /* add it to the list */
            _gc_list_append (element);
        }

        return ptr;
    }

Concernant la réallocation, que doit-on faire ?

* rechercher le pointeur dans notre liste ;
* si on ne le trouve pas, on retourne NULL ;
* si on le trouve, on réalloue avec ``realloc()`` ;
* on retourne le nouveau pointeur.

Ce qui donne :

    :::c
    void *gc_realloc (void *addr, size_t sz)
    {
        struct gc_list_t *element = NULL;

        /* if addr is NULL, treat it as allocation */
        if (addr == NULL)
        {
            return gc_alloc (sz);
        }

        /* if size is 0, treat it as unref */
        if (sz == 0)
        {
            gc_unref (addr);
            return NULL;
        }

        /* so now, search the address in the list */
        element = _gc_list_find (addr);

        /* element found */
        if (element != NULL)
        {
            /* realloc it */
            void *ptr = realloc (addr, sz);

            /* if it doesn't fail */
            if (ptr != NULL)
            {
                /* assign the new pointer (NB: realloc may return a different pointer) */
                element->addr = ptr;
                return ptr;
            }

            /* when realloc returns NULL, the old pointer isn't freed or moved,
             * so we don't remove it from our list.
             */
        }

        /* element not found */

        return NULL;
    }

### Référencement, déréférencement

Le référencement est plutôt simple :

* on cherche le pointeur dans notre liste ;
* si on ne le trouve pas, on retourne NULL ;
* si on le trouve, on incrémente son compteur de référence ;
* on retourne le pointeur.

On obtient donc :

    :::c
    void *gc_ref (void *addr)
    {
        if (addr != NULL)
        {
            struct gc_list_t *element = _gc_list_find (addr);

            if (element != NULL)
            {
                element->refcount++;
                return element->addr;
            }
        }

        return NULL;
    }

Pour le déréférencement, c'est sensiblement pareil :

* on cherche le pointeur dans notre liste ;
* si on ne le trouve pas, on fait rien ;
* si on le trouve on décrémente son compteur de référence ;
* si le compteur de référence atteint 0, alors on libère la mémoire.

Ce qui donne :

    :::c
    void gc_unref (void *addr)
    {
        struct gc_list_t *element = NULL;

        /* if address is NULL, do nothing */
        if (addr == NULL)
        {
            return;
        }

        element = _gc_list_find (addr);

        /* if the element isn't found, do nothing */
        if (element == NULL)
        {
            return;
        }

        /* now decrement refcount */
        element->refcount--;

        /* if refcount reaches 0 */
        if (element->refcount == 0)
        {
            /* free memory */
            free (element->addr);

            _gc_list_remove (element);
        }
    }

## Conclusion

Voilà un petit _Garbage Collector_ qui vous facilitera la vie, et qui vous en apprend un peu plus sur le fonctionnement de ce dernier.

Vous devez toujours gérer votre mémoire, ne pas la laisser s'entasser comme des poubelles qu'on ne sort pas. Le _GC_ vous aide à la gérer, il ne le fait pas à votre place.
