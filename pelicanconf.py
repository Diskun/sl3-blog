#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Site
AUTHOR = 'SL3 Team'
SITENAME = 'SUPINFO Low-Level Laboratory'
SITEURL = 'http://www.lab-sl3.org' # no trailing slash at the end !
DISQUS_SITENAME = 'lab-sl3'

# Language, Date format, Timezone...
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'fr'

# Navigation
DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False

MENUITEMS = ((u'Accueil', SITEURL),
             (u'Présentation', '%s/pages/presentation-du-sl3.html' % SITEURL),
             (u'Contribuer', '%s/pages/contribuer-au-sl3.html' % SITEURL),
             (u'Forums', '%s/forums/' % SITEURL),
             (u'Reddit', 'http://www.reddit.com/r/sl3'),)

# Look
THEME = 'notmyidea-light-fr'

# Articles
DEFAULT_CATEGORY = 'news'
DEFAULT_PAGINATION = 8

ARTICLE_URL = 'articles/{category}/{slug}.html'
ARTICLE_SAVE_AS = 'articles/{category}/{slug}.html'

ARTICLE_LANG_URL = 'articles/{category}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = 'articles/{category}/{slug}-{lang}.html'

# Feed
FEED_DOMAIN = SITEURL

FEED_ATOM = None
FEED_RSS = 'feeds/rss.xml'

# Feed regardless of language
FEED_ALL_ATOM = None
FEED_ALL_RSS = 'feeds/all.rss.xml'

# Feeds by category
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = 'feeds/%s.rss.xml'

# Feeds by language
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = 'feeds/all-%s.rss.xml'

# Social links
# - actually, atom and rss feeds links will not be displayed if SOCIAL is empty
SOCIAL = (('twitter', 'https://twitter.com/lab_sl3'),)

# Blogroll
LINKS = ()

# Static files
FILES_TO_COPY = (('extra/favicon.ico', 'favicon.ico'),
                 ('extra/guide-contribution.pdf', 'guide-contribution.pdf'))
